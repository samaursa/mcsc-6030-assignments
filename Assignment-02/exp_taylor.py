import math
import numpy

TOL = 1.0e-16

def exp_taylor(x, N_terms):

    # if convergence is -1, then the sum has not converged yet
    convergence = numpy.empty(len(x), numpy.int64)
    convergence[:] = 1

    mask = numpy.empty(len(x), bool)
    mask[:] = False

    # store all the sums
    sum = numpy.empty(len(x), numpy.float64)
    sum[:] = 1

    delta = numpy.empty(len(x), numpy.float64)
    delta[:] = 1

    for k in range(1, N_terms+1):
        convergence = numpy.where(mask == False, k, convergence)
        currDelta = abs(x)/float(k)
        delta = delta * currDelta
        sum = numpy.where(mask == False,  sum + delta, sum)
        mask = numpy.where(sum*TOL < delta, False, True)

    sum = numpy.where(x < 0, 1/sum, sum)
    return sum, convergence

def test_exp_taylor():
    N_max = 100
    R = 20  
    step = 4
    header = "x".center(10) + "# terms".rjust(8) + "true".rjust(19) 
    header += "approximate".rjust(19) + "error".rjust(19) 
    print header
    print "="*(10+19+19+19+8)
    xRange = numpy.arange(-R, R+1, step)
    (y, N_terms) = exp_taylor(xRange, N_max)

    counter = 0
    for ell in range(-R,R+1,step):
        x = float(ell)
        currY = y[counter]
        exp_true = math.exp(x)
        relative_error = abs( (currY-exp_true) / exp_true )
        print "%10.5f%8d%19g%19g%19g" % (x,N_terms[counter],exp_true,currY,relative_error)
        counter += 1

if __name__=='__main__':
    test_exp_taylor()
