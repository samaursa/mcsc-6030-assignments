#include <math.h>
/* f(x;\alpha) = \log(\alpha x) / \sqrt{x} 
   where \alpha is some positive parameter. */
double f (double x, void * params);

double f_cos_sq_exp(double x, void *);