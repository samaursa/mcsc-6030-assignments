#include <stdio.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_math.h>
#include "function.h"

extern double f (double, void*);

int
main (void)
{
  /* Claim memory needed for adaptive integration */
  gsl_integration_workspace * w 
    = gsl_integration_workspace_alloc (1000);
  
  /* Construct GSL function object to describe solution */
  gsl_function F;
  F.function = &f_cos_sq_exp;
  double alpha = 1.0;
  F.params = &alpha;

  /* Parameters to control quadrature routine */
  double a, b, tol_abs, tol_rel;
  int limit;
  a = 0.0;
  b = 3.0;
  tol_abs = 0;
  tol_rel = 1.0e-7;
  limit = 1000;
  
  /* Actual call to quadrature routine */
  double result, error;
  // use 61 point Gauss-Kronrod rule to get a more accurate estimate
  gsl_integration_qag (&F, a, b, tol_abs, tol_rel, limit, GSL_INTEG_GAUSS61, 
                       w, &result, &error); 
  
  // test our cos^2(exp^x) gsl function
  gsl_function F_cos_exp;
  F_cos_exp.function = &f_cos_sq_exp;
  F_cos_exp.params = NULL;
  GSL_FN_EVAL(&F_cos_exp, 2);
  
  printf ("result          = % .18f\n", result);
  printf ("estimated error = % .18f\n", error);
  printf ("# intervals     = %d\n", (int) w->size);

  /* Return memory allocated for solver */
  gsl_integration_workspace_free (w);

  return 0;
}
