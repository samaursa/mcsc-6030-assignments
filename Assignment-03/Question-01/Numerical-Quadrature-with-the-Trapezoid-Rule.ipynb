{
 "metadata": {
  "name": ""
 },
 "nbformat": 3,
 "nbformat_minor": 0,
 "worksheets": [
  {
   "cells": [
    {
     "cell_type": "heading",
     "level": 1,
     "metadata": {
      "slideshow": {
       "slide_type": "subslide"
      }
     },
     "source": [
      "Numerical Quadrature with the trapezoid rule"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Numerical quadrature refers to approximating a definite integral numerically, \n",
      "$$~~ \\int_a^b f(x) dx.$$\n",
      "Many numerical analysis textbooks describe a variety of quadrature methods or \"rules\".  "
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "First define a simple function for which we know the exact antiderivative:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "import numpy as np\n",
      "import matplotlib.pyplot as mpl\n",
      "%matplotlib inline\n",
      "\n",
      "def f1(x):\n",
      "    return 1. + x**3\n",
      "\n",
      "a1 = 0.\n",
      "b1 = 2.\n",
      "int_true1 = (b1-a1) + (b1**4 -a1**4) / 4.\n",
      "print \"true integral: %22.14e\" % int_true1"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "heading",
     "level": 2,
     "metadata": {},
     "source": [
      "The Trapezoid Rule"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "We will first look at the Trapezoid method.  This method is implemented by evaluating the function at $n$ points and then computing the areas of the trapezoids defined by a piecewise linear approximation to the original function defined by these points.  In the figure below, we are approximating the integral of the blue curve by the sum of the areas of the red trapezoids."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "def plot_trap(f,a,b,n):\n",
      "    x = np.linspace(a-0.2, b+0.2, 10000) # grid points for smooth plot\n",
      "    mpl.plot(x,f(x),'b-')\n",
      "    xj = np.linspace(a,b,n)\n",
      "    mpl.plot(xj,f(xj),'ro-')\n",
      "    for xi in xj:\n",
      "        mpl.plot([xi,xi], [0,f(xi)], 'r')\n",
      "    mpl.plot([a,b], [0,0], 'r') # plot line along x-axis\n",
      "    mpl.show()\n",
      "\n",
      "plot_trap(f1,a1,b1,5)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "heading",
     "level": 3,
     "metadata": {},
     "source": [
      "The Trapezoid rule formula"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "The area of a single trapezoid is the width of the base times the average height, so between points $x_j$ and $x_{j+1}$ this gives:\n",
      "$$ \\frac{h}{2} (f(x_j) + f(x_{j+1}). $$\n",
      "\n",
      "Summing this up over all the trapezoids gives:\n",
      "$$ h\\left(\\frac 1 2 f(x_0) + f(x_1) + f(x_2) + \\cdots + f(x_{n-2}) + \\frac 1 2 f(x_{n-1})\\right) = h\\sum_{j=0}^{n-1} f(x_j) - \\frac h 2 \\left(f(x_0) + f(x_{n-1})\\right) =  h\\sum_{j=0}^{n-1} f(x_j) - \\frac h 2 \\left(f(a) + f(b))\\right). $$\n",
      "\n",
      "This can be implemented as follows (note that in Python fj[-1] refers to the last element of fj, and similarly fj[-2] would be the next to last element)."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "def trapezoid(f,a,b,n):\n",
      "    h = (b-a)/(n-1)\n",
      "    xj = np.linspace(a,b,n)\n",
      "    fj = f(xj)\n",
      "    int_trapezoid = h*np.sum(fj) - 0.5*h*(fj[0] + fj[-1])\n",
      "    return int_trapezoid\n"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "We can test it out for the points used in the figure above:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "n = 5\n",
      "int_trap = trapezoid(f1,a1,b1,n)\n",
      "error = abs(int_trap - int_true1)\n",
      "print \"trapezoid rule approximation: %22.14e,  error: %10.3e\" % (int_trap, error)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Using more points will give a better approximation, try changing it in the cell above."
     ]
    },
    {
     "cell_type": "heading",
     "level": 3,
     "metadata": {},
     "source": [
      "Convergence tests"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "If we increase $n$, the number of points used, and hence decrease $h$, the spacing between points, we expect the error to converge to zero for reasonable functions $f(x)$.\n",
      "\n",
      "The trapezoid rule is \"second order accurate\", meaning that the error goes to zero like $O(h^2)$ for a function that is sufficiently smooth (for example if its second derivative is continuous).  For small $h$, the error is expected to be behave like $Ch^2 + O(h^3)~$ as $h$ goes to zero, where $C$ is some constant that depends on how smooth $h$ is.  \n",
      "\n",
      "If we double $n$ (and halve $h$) then we expect the error to go down by a factor of 4 roughly (from $Ch^2$ to $C(h/2)^2~$).\n",
      "\n",
      "We can check this by trying several values of n and making a table of the errors and the ratio from one n to the next:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "def error_table(f,a,b,nvals,int_true):\n",
      "    print \"      n         trapezoid            error       ratio\"\n",
      "    last_error = 0.  # need something for first ratio\n",
      "    for n in nvals:\n",
      "        int_trap = trapezoid(f,a,b,n)\n",
      "        error = abs(int_trap - int_true)\n",
      "        ratio = last_error / error\n",
      "        last_error = error # for next n\n",
      "        print \"%8i  %22.14e  %10.3e  %10.3e\" % (n,int_trap, error, ratio)\n",
      "    \n",
      "nvals = np.array([5, 10, 20, 40, 80, 160, 320])\n",
      "error_table(f1,a1,b1,nvals,int_true1)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "(Note that the first ratio reported is meaningless.)\n",
      "\n",
      "Convergence might be easier to see in a plot.  If a method is p'th order accurate then we expect the error to behave like $E\\approx Ch^p$ for some constant $C$, for small $h$.  This is hard to visualize.  It is much easier to see what order accuracy we are achieving if we produce a log-log plot instead, since $E = Ch^p~$ means that $\\log E = \\log C + p\\log h$ \n",
      "\n",
      "In other words $\\log E~$ is a linear function of $\\log h~$."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "def error_plot(f,a,b,nvals,int_true):\n",
      "    errvals = np.zeros(nvals.shape)  # initialize to right shape\n",
      "    for i in range(len(nvals)):\n",
      "        n = nvals[i]\n",
      "        int_trap = trapezoid(f,a,b,n)\n",
      "        error = abs(int_trap - int_true)\n",
      "        errvals[i] = error\n",
      "    hvals = (b - a) / (nvals - 1)  # vector of h values for each n\n",
      "    mpl.loglog(hvals,errvals, 'o-')\n",
      "    mpl.xlabel('spacing h')\n",
      "    mpl.ylabel('error')\n",
      "    mpl.show()\n",
      "    \n",
      "error_plot(f1,a1,b1,nvals,int_true1)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "heading",
     "level": 3,
     "metadata": {},
     "source": [
      "An oscillatory function"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "If the function $f(x)$ is not as smooth (has larger second derivative at various places) then the accuracy with a small number of points will not be nearly as good.  For example, consider this function:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "k = 50.\n",
      "def f2(x):\n",
      "    return 1. + x**3 + np.sin(k*x)\n",
      "\n",
      "a2 = 0.\n",
      "b2 = 2.\n",
      "int_true2 = (b2-a2) + (b2**4 - a2**4) / 4. - (1./k) * (np.cos(k*b2) - np.cos(k*a2))\n",
      "print \"true integral: %22.14e\" % int_true2"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Making k larger will make it more oscillatory.  For this function with k=50, using n=10 points is not going to give a very good approximation:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "plot_trap(f2,a2,b2,10)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "This doesn't look very good, but for larger values of $n$, we still see the expected convergence rate:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "error_plot(f2,a2,b2,nvals,int_true2)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Now make the function much more oscillatory with a larger value of $k$..."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "k = 1000.\n",
      "def f2(x):\n",
      "    return 1. + x**3 + np.sin(k*x)\n",
      "\n",
      "a2 = 0.\n",
      "b2 = 2.\n",
      "int_true2 = (b2-a2) + (b2**4 - a2**4) / 4. - (1./k) * (np.cos(k*b2) - np.cos(k*a2))\n",
      "print \"true integral: %22.14e\" % int_true2"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "For the previous choice of nvals the method does not seem to be doing well:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "nvals = np.array([5, 10, 20, 40, 80, 160, 320])\n",
      "print \"nvals = \",nvals\n",
      "error_plot(f2,a2,b2,nvals,int_true2)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "In this case the $O(h^2)~$ behavior does not become apparent unless we use much smaller $h$ values (i.e., much larger values of $n$) so that we are resolving the oscillations:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "nvals = np.array([5 * 2**i for i in range(12)])\n",
      "print \"nvals = \",nvals\n",
      "error_plot(f2,a2,b2,nvals,int_true2)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Eventually, now that we have sufficiently large values of $n$, we see second-order convergence and ratios that approach 4:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "error_table(f2,a2,b2,nvals,int_true2)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    }
   ],
   "metadata": {}
  }
 ]
}