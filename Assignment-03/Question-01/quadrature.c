#include "quadrature.h"
#include <stdlib.h>
#include <stdio.h>

double trapezoid (double (*fp)(double), double a, double b, int N)
{
  double h = (b - a)/(N - 1.0);
  
  double xj_sum = 0.0;
  double xj_begin, xj_end;
  
  for (int i = 0; i < N; ++i)
  { 
    double f_res = fp(h * i);
    
    if (i == 0)     { xj_begin = f_res; }
    if (i == N - 1) { xj_end   = f_res; }
    
    xj_sum += f_res; 
  }
  
  double int_trapezoid = h * xj_sum - 0.5 * h * (xj_begin + xj_end);
  
  return int_trapezoid;
}

void error_table (double (*fp)(double), double a, double b,
                  int nrows, int nvals[], double int_true)
{
  printf("       n         trapezoid            error       ratio\n");
  double last_error = 0.0;
  for (int i = 0; i < nrows; ++i)
  {
    double int_trap = trapezoid(fp, a, b, nvals[i]);
    double error    = fabs(int_trap - int_true);
    double ratio    = last_error / error;
    last_error      = error;
    printf("%8d   %22.14e  %10.3e  %10.3e\n", nvals[i], int_trap, error, ratio);
  }
}
