#include "quadrature.h"
#include <math.h>

static const double k_const = 1000.0;

/* Here, f1 is a specific real-valued function of a real variable */
double
f1 (double x)
{ return 1.0 + pow (x, 3); /* 1 + x**3 */ }

double
f2 (double x)
{ return 1.0 + pow (x, 3) + sin(k_const * x); }

int
main (int argc, char *argv[])
{
  const int nrows = 12; /* Number of rows in the table (and length of nvals) */
  
  double a, b, int_true;
  int nvals[nrows], k;
  double (*fp) (double); /* fp is a function pointer */

  fp = f1; /* Assign function pointer fp as f1 */
  a = 0.0; /* Left endpoint of integration interval */
  b = 2.0; /* Right endpoint of integration interval */
  // int_true2 = (b2-a2) + (b2**4 - a2**4) / 4. - (1./k) * (np.cos(k*b2) - np.cos(k*a2))
  int_true = (b - a) + (pow (b, 4) - pow (a, 4)) / 4.0 - (1.0/k_const) * (cos(k_const*b) - cos(k_const * a));
  printf ("true integral: %22.14e\n\n", int_true);

  /* Fill in array of values of N for trapezoidal integration */
  for (k = 0; k < nrows; k++)
  { nvals[k] = 5 * pow (2, k); }

  /* You have to write the function error_table in quadrature.c */
  error_table (f2, a, b, nrows, nvals, int_true);

  return 0;
}