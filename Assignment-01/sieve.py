import sys


class PotentialPrime:
    num = 0
    prime = False

    def __init__(self, a_num, a_prime):
        self.num = a_num
        self.prime = a_prime

# -------------------------------------------------------------------------
# command line arguments

upLimit = 0
showHelp = False
noPrint = False
if len(sys.argv) == 2:
    if sys.argv[1] == "-help":
        showHelp = True
    else:
        try:
            upLimit = int(sys.argv[1])
        except ValueError:
            upLimit = -1
elif len(sys.argv) == 3:
    if sys.argv[1] != "-noprint":
        print 'Invalid first command line argument (%s)' % sys.argv[1]
        showHelp = True
    else:
        noPrint = True
        upLimit = int(sys.argv[2])

if upLimit == -1:
    print 'Last argument must be an integer'
    showHelp = True

if 0 <= upLimit < 2 and showHelp is not True:
    print 'No upper limit specified OR upper limit < 2'
    showHelp = True

if showHelp is True:
    print 'Example usage: python sieve -noprint 100'
    sys.exit()

# -------------------------------------------------------------------------

numRange = range(0, upLimit + 1) # start from 0 to keep proper indexes
intList = []

for i in numRange:
    intList.append(PotentialPrime(i, True))

primeCount = 0
for counter, currInt in enumerate(intList):
    if counter >= 2 and currInt.prime is True:
        for index in range(currInt.num, (upLimit // currInt.num) + 1):
            intList[index * currInt.num].prime = False
        primeCount += 1
        if noPrint is False:
            print currInt.num

print 'Total number of prime numbers in range [2, %ld]: %ld' % (upLimit, primeCount)
