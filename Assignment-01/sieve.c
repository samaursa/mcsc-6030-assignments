#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <memory.h>

typedef long int lint;

static bool noprint = false;
static bool help    = false;
static lint upLimit = 0;

void ProcessCommandLineArguments(int argc, char** argv)
{
  for (int i = 1; i < argc; i++)
  {
    if (strcmp(argv[i], "-noprint") == 0)  /* Process optional arguments. */
    { noprint = true; }
    else if(strcmp(argv[i], "-help") == 0)
    { help = true; }
    else
    {
      READ_INTEGER:
      upLimit = strtol(argv[i], NULL, 0);

      if (upLimit == 0)
      { printf("Invalid upper limit specified\n"); }
    }
  }
}

typedef struct PotentialPrime
{
  lint  m_num;
  bool  m_isPrime;
} PotentialPrime;

int main(int argc, char** argv)
{
  // the last argument is considered the upper limit
  ProcessCommandLineArguments(argc, argv);

  if (upLimit < 2 && help != true)
  {
    printf("No upper limit specified OR upper limit < 2\n");
    help = true;
  }

  if (help == true)
  { printf("Example usage: ./sieve.o -noprint 100\n"); return 0; }

  // assuming upper limit is inclusive
  upLimit++;

  PotentialPrime* intList = malloc(sizeof(*intList) * upLimit);
  for (lint i = 0; i < upLimit; ++i)
  {
    intList[i].m_num = i;
    intList[i].m_isPrime = true;
  }

  lint numPrimes = 0;
  for (lint i = 0; i < upLimit; ++i)
  {
    if (i < 2 || intList[i].m_isPrime != true)
    { continue; }

    const lint currNum = intList[i].m_num;
    const lint nestedLimit = upLimit / currNum;

    for (lint j = currNum; j <= nestedLimit; ++j)
    { intList[j*currNum].m_isPrime = false; }

    numPrimes++;
    if (noprint == false)
    { printf(" %ld", intList[i].m_num); }
  }

  printf("\nTotal number of prime numbers in range [2, %ld]: %ld\n", upLimit - 1, numPrimes);

  return 0;
}
