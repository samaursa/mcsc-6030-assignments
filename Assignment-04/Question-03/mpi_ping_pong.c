#include "mpi/mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main(int argc, char* argv[])
{
  MPI_Init(&argc, &argv);
  
  int numTasks, rank;
  MPI_Comm_size(MPI_COMM_WORLD, &numTasks);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if (numTasks < 2)
  {
    if (rank == 0) { printf("\nError: Need 2 processes"); }
    MPI_Finalize();
    return -1;
  }
  else if (numTasks > 2)
  {
    if (rank == 0) { printf("\nWarn: Need only 2 processes"); }
  }
  
  const int numBytes = 10000;
  // the chars in the data don't matter
  char* data = malloc(numBytes * sizeof(char));

  const int numSamples = 10000;
  const int messageTag = 1;
  MPI_Status stat;
  
  double avgLat = 0.0;
  for (int i = 0; i < numSamples; ++i)
  {
    // assuming latency is time it takes to send 0 bytes and bandwidth is the
    // number of bytes sent per second...
    if (rank == 0)
    {
      int dest = 1, src = 1;
      
      double t = MPI_Wtime();
      MPI_Send(data, 0, MPI_CHAR, dest, messageTag, MPI_COMM_WORLD);
      MPI_Recv(data, 0, MPI_CHAR, dest, messageTag, MPI_COMM_WORLD, &stat);
      avgLat += (MPI_Wtime() - t);
      
    }
    else if (rank == 1)
    {
      int dest = 0, src = 0;
      
      MPI_Recv(data, 0, MPI_CHAR, dest, messageTag, MPI_COMM_WORLD, &stat);
      MPI_Send(data, 0, MPI_CHAR, dest, messageTag, MPI_COMM_WORLD);
    }
  }
  
  double avgBand = 0.0;
  for (int i = 0; i < numSamples; ++i)
  {
    // assuming latency is time it takes to send 0 bytes and bandwidth is the
    // number of bytes sent per second...
    double lambda = 0.0, beta = 0.0;
    
    if (rank == 0)
    {
      int dest = 1, src = 1;
      
      double t = MPI_Wtime();
      MPI_Send(data, numBytes, MPI_CHAR, dest, messageTag, MPI_COMM_WORLD);
      MPI_Recv(data, numBytes, MPI_CHAR, dest, messageTag, MPI_COMM_WORLD, &stat);
      avgBand += (numBytes * 2)/ (MPI_Wtime() - t);
      
    }
    else if (rank == 1)
    {
      int dest = 0, src = 0;
      
      MPI_Recv(data, numBytes, MPI_CHAR, dest, messageTag, MPI_COMM_WORLD, &stat);
      MPI_Send(data, numBytes, MPI_CHAR, dest, messageTag, MPI_COMM_WORLD);
    }
  }
  
  if (rank == 0)
  {
    avgLat  /= (double)numSamples;
    avgBand /= (double)numSamples;
    
    printf("\nWith %d samples and %d bytes of data", numSamples, numBytes);
    printf("\navg latency  : %.6e sec", avgLat);
    printf("\navg bandwidth: %.6e bytes/sec", avgBand);
  }
  
  MPI_Finalize();
}
