from mpi4py import MPI
import numpy as np

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

numSamples = 10000
messageTag = 1

numBytes = 10000
data    = np.arange(numBytes, dtype='b')
noData  = np.arange(1, dtype='b')

avgLat = 0.0
for i in range(0, numSamples):
    if rank == 0:
        dest = 1
        src = 1
        t = MPI.Wtime()
        comm.Send([noData, MPI.CHAR], dest=dest, tag=messageTag)
        comm.Recv([noData, MPI.CHAR], source=src, tag=messageTag)
        avgLat += MPI.Wtime() - t
    elif rank == 1:
        dest = 0
        src = 0
        comm.Recv([noData, MPI.CHAR], source=src, tag=messageTag)
        comm.Send([noData, MPI.CHAR], dest=dest, tag=messageTag)

avgBand = 0.0
for i in range(0, numSamples):
    if rank == 0:
        dest = 1
        src = 1
        t = MPI.Wtime()
        comm.Send([data, MPI.CHAR], dest=dest, tag=messageTag)
        comm.Recv([data, MPI.CHAR], source=src, tag=messageTag)
        avgBand += (numBytes * 2) / (MPI.Wtime() - t)
    elif rank == 1:
        dest = 0
        src = 0
        comm.Recv([data, MPI.CHAR], source=src, tag=messageTag)
        comm.Send([data, MPI.CHAR], dest=dest, tag=messageTag)

if rank == 0:
    avgLat  /= numSamples
    avgBand /= numSamples
    print "With %d sampels and %d bytes of data" % (numSamples, numBytes)
    print "avg latency  : %.6e sec" % (avgLat)
    print "avg bandwidth: %.6e bytes/sec" % (avgBand)
