#include "mpi/mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#define PRINT_MASTER

#ifdef PRINT_MASTER
# define PRINTF_MASTER(_str_, ...) printf(_str_, __VA_ARGS__)
#else
# define PRINTF_MASTER(_str_, ...)
#endif

#ifdef PRINT_SLAVE
# define PRINTF_SLAVE(_str_, ...) printf(_str_, __VA_ARGS__)
#else
# define PRINTF_SLAVE(_str_, ...)
#endif

double root_func(double x)
{
  double sum = 0.0;
  for (int k = 1; k <= 1000; ++k)
  {
    double x_pow_k = pow(x, k);
    sum += sin(x_pow_k);
  }
  
  return -2.0 + sum;
}

typedef struct interval
{
  double start;
  double end;
}interval;

interval get_new_interval(double const * a_intervals, double const * a_values, 
                          const int a_count)
{
  interval newInter;
  newInter.start = 0.0;
  newInter.end   = 0.0;
  
  for (int i = 0; i < a_count - 1; ++i)
  {
    if (a_values[i] < 0.0 && a_values[i+1] > 0.0)
    {
      newInter.start  = a_intervals[i];
      newInter.end    = a_intervals[i+1];
      break;
    }
  }
  
  return newInter;
}

int main(int argc, char* argv[])
{
  MPI_Init(&argc, &argv);
  
  MPI_Comm MPIWorld = MPI_COMM_WORLD;
  
  int numProcs, rank;
  MPI_Comm_size(MPIWorld, &numProcs);
  MPI_Comm_rank(MPIWorld, &rank);
  
  if (numProcs < 4) 
  { 
    if (rank == 0) { printf("\nRequire minimum of 4 processes\n"); }
    MPI_Finalize();
    return -1;
  }
  
  const int     numAvailProcs       = numProcs - 1;
  const int     masterProc          = 0;
  const int     work_tag            = 1;
  const int     stop_tag            = 2;
  const double  minSubintervalWidth = pow(10.0, -11.0);
  
  interval      currInterval;
  currInterval.start                = 0.0;
  currInterval.end                  = 1.0;
  double        subintervalWidth    = 1.0;
  
  double* results   = (double*)malloc(numAvailProcs * sizeof(double));
  double* intervals = (double*)malloc(numAvailProcs * sizeof(double));
  
  if (rank == masterProc)
  {
    while (subintervalWidth >= minSubintervalWidth)
    {
      const double delta = subintervalWidth / (numAvailProcs - 1);
      
      for (int dest = 1; dest <= numAvailProcs; ++dest)
      {
        const double step = (double)(dest - 1) * delta;
        double xVal       = currInterval.start + step;
        intervals[dest-1] = xVal;
        MPI_Send(&xVal, 1, MPI_DOUBLE, dest, work_tag, MPI_COMM_WORLD);
      }
      
      MPI_Status stat;
      for (int src = 1; src <= numAvailProcs; ++src)
      {
        MPI_Recv(&results[src-1], 1, MPI_DOUBLE, src, work_tag, MPI_COMM_WORLD, &stat);
      }
      
      currInterval      = get_new_interval(intervals, results, numAvailProcs);
      subintervalWidth  = currInterval.end - currInterval.start;
      PRINTF_MASTER("\nNew interval: [%.6e, %.6e] with width: %.6e", currInterval.start, currInterval.end, subintervalWidth);
    }
    
    for (int dest = 1; dest <= numAvailProcs; ++dest)
    {
      MPI_Send(NULL, 0, MPI_CHAR, dest, stop_tag, MPI_COMM_WORLD);
    }
    
    double approxZero = root_func(currInterval.start);
    printf("\nApproximate zero of f(x): %.6e with x: %.11e\n", approxZero, currInterval.start);
  }
  else
  {
    MPI_Request stopRequest, workRequest;
    MPI_Irecv(NULL, 0, MPI_CHAR, masterProc, stop_tag, MPI_COMM_WORLD, &stopRequest);
    
    double xVal;
    MPI_Irecv(&xVal, 1, MPI_DOUBLE, masterProc, work_tag, MPI_COMM_WORLD, &workRequest);
    
    int stop = 0;
    while(stop == 0)
    {
      int work = 0;
      MPI_Test(&workRequest, &work, MPI_STATUS_IGNORE);
      if (work)
      {
        double res = root_func(xVal);
        MPI_Send(&res, 1, MPI_DOUBLE, masterProc, work_tag, MPI_COMM_WORLD);
        MPI_Irecv(&xVal, 1, MPI_DOUBLE, masterProc, work_tag, MPI_COMM_WORLD, &workRequest);
      }
      
      MPI_Test(&stopRequest, &stop, MPI_STATUS_IGNORE);
    }
  }
  
  MPI_Finalize();
}