import numpy as np
from mpi4py import MPI

def root_func(x):
    r           = np.arange(1.0, 1000.0+1.0)
    xpow        = np.full([1000.0], x)
    xpowr       = np.power(xpow, r)
    xpowr_sin   = np.sin(xpowr)
    sum         = np.sum(xpowr_sin)
    return -2. + sum

def get_new_interval(a_intervals, a_values):
    rangeToRet = np.array([0.0, 0.0])
    for i in range(0, len(a_values) - 1):
        if (a_values[i] < 0.0 and a_values[i+1] > 0.0):
            rangeToRet[0] = a_intervals[i];
            rangeToRet[1] = a_intervals[i+1];
            break
    return rangeToRet

comm     = MPI.COMM_WORLD
rank     = comm.Get_rank()
numProcs = comm.Get_size()

numAvailProcesses   = numProcs - 1;
masterProc          = 0
work_tag            = 1
stop_tag            = 2
minSubIntervalWidth = pow(10.0, -11.0)

currInterval        = np.array([0.0, 1.0])
subIntervalWidth    = 1.0

results             = np.zeros(numAvailProcesses)
intervals           = np.zeros(numAvailProcesses)

if (rank == masterProc):
    while subIntervalWidth >= minSubIntervalWidth:
        delta = subIntervalWidth / float(numAvailProcesses - 1)
        for dest in range(1, numAvailProcesses+1):
            step = (float(dest)-1.0) * delta
            xVal = currInterval[0] + step;
            intervals[dest-1] = xVal;
            comm.send(xVal, dest=dest, tag=work_tag)

        for src in range(1, numAvailProcesses+1):
            retVal = comm.recv(source=src, tag=work_tag)
            results[src-1] = retVal

        currInterval        = get_new_interval(intervals, results)
        subIntervalWidth    = currInterval[1] - currInterval[0]

        print "New interval: [%.6e, %.6e] with width: %.6e" % (currInterval[0], currInterval[1], subIntervalWidth)

    for dest in range(1, numAvailProcesses+1):
        comm.send(0, dest=dest, tag=stop_tag)

    approxZero = root_func(currInterval[0])
    print "Approximate zero of f(x): %.6e with x: %.11e" % (approxZero, currInterval[0])
else:
    while not comm.Iprobe(source=masterProc, tag=stop_tag):
        if comm.Iprobe(source=masterProc, tag=work_tag):
            xVal = comm.recv(source=masterProc, tag=work_tag)
            res  = root_func(xVal)
            comm.send(res, dest=masterProc, tag=work_tag)
