#include "mpi/mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include "cblas.h"

#define PRINT_MASTER

#ifdef PRINT_MASTER
# define PRINTF_MASTER(_str_, ...) printf(_str_, __VA_ARGS__)
#else
# define PRINTF_MASTER(_str_, ...)
#endif

#ifdef PRINT_SLAVE
# define PRINTF_SLAVE(_str_, ...) printf(_str_, __VA_ARGS__)
#else
# define PRINTF_SLAVE(_str_, ...)
#endif

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))

void print_matrix(int n_rows, int n_cols, double* a_m)
{
  printf("\n[");
  for (int row = 0; row < n_rows; ++row)
  {
    printf("\n[");
    for (int col = 0; col < n_cols; ++col)
    {
      printf("%.6e, ", a_m[n_cols * row + col]);
    }
    printf("]\n");
  }
  printf("]");
}

void print_vector(int n_cols, double* a_v)
{
  printf("\n[");
  for (int col = 0; col < n_cols; ++col)
  {
    printf("%.6e, ", a_v[col]);
  }
  printf("]");
}

void master_process(int n_rows, int n_cols, MPI_Comm a_comm, int n_proc)
{
  double*  x = malloc(n_cols * sizeof(double));
  double*  A = malloc(n_cols * n_rows * sizeof(double));
  double*  y = malloc(n_rows * sizeof(double));
  double ans;
  
  for (int i = 1; i <= n_cols; ++i)
  { x[i-1] = i; }
  
  for (int k = 0; k < n_rows; ++k)
  { 
    for (int col = 0; col < n_cols; ++col)
    { A[n_cols * k + col] = k+1; }
  }
  
  MPI_Bcast(x, n_cols, MPI_DOUBLE, 0, a_comm);
  
  int n_sent = 0;
  for (int k = 1; k <= MIN(n_rows, n_proc-1); ++k)
  {
    MPI_Send(&A[n_cols * n_sent], n_cols, MPI_DOUBLE, k, n_sent, a_comm);
    ++n_sent;
  }
  
  for (int k = 0; k < n_rows; ++k)
  {
    MPI_Status stat;
    MPI_Recv(&ans, 1, MPI_DOUBLE, MPI_ANY_SOURCE, MPI_ANY_TAG, a_comm, &stat);
    
    int sender = stat.MPI_SOURCE;
    int row    = stat.MPI_TAG;
    
    y[row] = ans;
    
    if (n_sent < n_rows)
    {
      MPI_Send(&A[n_cols * n_sent], n_cols, MPI_DOUBLE, sender, n_sent, a_comm);
      ++n_sent;
    }
    else
    {
      MPI_Send(y, n_rows, MPI_DOUBLE, sender, n_rows+1, a_comm);
    }
  }
  
  double*  A_dot_x = malloc(n_rows * sizeof(double));
  
  printf("\nFinished!");
  cblas_dgemv(CblasRowMajor, CblasNoTrans, n_rows, n_cols, 1.0, A, n_cols, x, 1, 1.0, A_dot_x, 1);
  
  //print_vector(n_rows, y);
  
  double* y_min_dot = malloc(n_rows * sizeof(double));
  memcpy(y_min_dot, y, sizeof(double) * n_rows);
  
  cblas_daxpy(n_rows, -1.0, A_dot_x, 1, y_min_dot, 1);
  
  double norm2 = cblas_dnrm2(n_rows, y_min_dot, 1);
  printf("\nCheck: ||y-Ax|| = %.6e\n", norm2);
}

void slave_process(int proc_id, int master_proc, int n_rows, int n_cols, MPI_Comm a_comm)
{
  double* x       = malloc(n_cols * sizeof(double));
  double* my_row  = malloc(n_cols * sizeof(double));
  double  ans;
  
  MPI_Status stat;
  
  MPI_Bcast(x, n_cols, MPI_DOUBLE, master_proc, a_comm);
  //print_vector(n_cols, x);
  MPI_Recv(my_row, n_cols, MPI_DOUBLE, master_proc, MPI_ANY_TAG, a_comm, &stat);
  //print_vector(n_cols, my_row);
  
  int row = stat.MPI_TAG;
  while (row < n_rows)
  {
    ans = cblas_ddot(n_cols, my_row, 1, x, 1);
    //printf("\nAns: %f", ans);
    MPI_Send(&ans, 1, MPI_DOUBLE, master_proc, row, a_comm);
    
    MPI_Recv(my_row, n_cols, MPI_DOUBLE, MPI_ANY_SOURCE, MPI_ANY_TAG, a_comm, &stat);
    row = stat.MPI_TAG;
  }
}

int main(int argc, char* argv[])
{
  MPI_Init(&argc, &argv);
  
  MPI_Comm MPIWorld = MPI_COMM_WORLD;
  
  int numProcs, rank;
  MPI_Comm_size(MPIWorld, &numProcs);
  MPI_Comm_rank(MPIWorld, &rank);
  
  if (numProcs < 2) 
  { 
    if (rank == 0) { printf("\nRequires two or more processes\n"); }
    MPI_Finalize();
    return -1;
  }
  
  const int     masterProc          = 0;
  const int     n_rows              = 4;
  const int     n_cols              = 10;
  
  if (rank == masterProc)
  {
    master_process(n_rows, n_cols, MPIWorld, numProcs);
  }
  else
  {
    slave_process(rank, masterProc, n_rows, n_cols, MPIWorld);
  }
  
  MPI_Finalize();
}