# **High Performance Computing Assignments Repository** #

## Author ##

**Saad Khattak** (100262910)

## Assignment 1 ##

`Python` version of the sieve algorithm works with `Python 2.7.6` while the `C` version has been compiled with the `-std=c99` flag.

## Assignment 2 ##

Each `Python` file can be run from command line using `python`. The `midpt_pi.py` requires a (non-optional) argument for number of regtangles used for the approximation of `pi`.

## Assignment 3 ##

Q1 has three parts. Each part can be built using the following arguments with make
* `make test1.exe`
* `make test2.exe`
* `make test2_omp.exe`

Running `test2_omp.exe` selects `2` threads by default. The executable takes 1 (optional) argument for the number of threads. For example, `test2_omp.exe 4` will select `4 OpenMP` threads.

Q2 solution can be built using `make`.
